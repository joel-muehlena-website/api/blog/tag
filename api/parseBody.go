package api

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"reflect"
	"time"

	"github.com/gorilla/schema"
)

func (api *ApiHandler) parseBody(contentType string, r *http.Request, i interface{}) error {

	if reflect.ValueOf(i).Kind() != reflect.Ptr {
		return fmt.Errorf("please pass the interface as pointer")
	}

	decoder := schema.NewDecoder()
	decoder.RegisterConverter(time.Time{}, func(s string) reflect.Value {
		layout := "2006-01-02T15:04:05"
		v, err := time.Parse(layout, s)
		if err == nil {
			return reflect.ValueOf(v)
		}

		return reflect.Value{}
	})

	if contentType == "application/json" {

		err := json.NewDecoder(r.Body).Decode(i)

		if err != nil {
			return err
		}

	} else if contentType == "application/x-www-form-urlencoded" {
		err := r.ParseForm()

		if err != nil {
			return err
		}

		err = decoder.Decode(i, r.PostForm)

		if err != nil {
			return err
		}
	} else {
		return errors.New("unsupported content type")
	}

	return nil
}
