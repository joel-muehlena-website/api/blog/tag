package api

import (
	"context"
	"encoding/json"
	"net/http"
	"net/url"
	"strconv"

	"github.com/georgysavva/scany/pgxscan"
	"github.com/gorilla/mux"
	"github.com/jackc/pgtype"
	"github.com/jackc/pgx/v4"
	"github.com/pkg/errors"
	"gitlab.com/joel-muehlena-website/api/blog/tag/types"
)

func (api *ApiHandler) getTagList(w http.ResponseWriter, r *http.Request) {

	nameQuery := r.URL.Query().Get("name")
	idQuery := r.URL.Query()["id[]"]

	if nameQuery != "" && len(idQuery) > 0 {
		api.writeError(&types.APIError{Err: errors.New("please speicify either the name or id[] query"), Code: 400, Message: "Wrong combination of query params"}, w)
		return
	}

	var err error
	if nameQuery != "" {
		nameQuery, err = url.QueryUnescape(nameQuery)

		if err != nil {
			api.writeError(&types.APIError{Err: errors.Wrap(err, "the query value was: "+nameQuery), Code: 500, Message: "failed to parse name query parameter"}, w)
			return
		}
	}

	var rows pgx.Rows
	ctx, cancel := context.WithTimeout(context.Background(), DEFAULT_QUERY_TIMEOUT)

	if len(idQuery) == 0 {
		rows, err = api.DBPool.Query(ctx, "SELECT * FROM tag WHERE name LIKE $1", "%"+nameQuery+"%")
	} else {

		idQueryNum := make([]int, 0)

		for _, query := range idQuery {
			num, err := strconv.Atoi(query)

			if err != nil {
				continue
			}

			idQueryNum = append(idQueryNum, num)
		}

		arr := &pgtype.Int4Array{}
		arr.Set(idQueryNum)

		rows, err = api.DBPool.Query(ctx, "SELECT * FROM tag WHERE id=ANY($1)", arr)
	}
	defer cancel()

	if err != nil {
		api.writeError(&types.APIError{Err: err, Code: 500, Message: "failed to query database"}, w)
		return
	}

	tags := make([]types.Tag, 0)
	for rows.Next() {
		var tag types.Tag
		err := pgxscan.ScanRow(&tag, rows)

		if err != nil {
			//TODO log error or something
			continue
		}

		tags = append(tags, tag)
	}

	json.NewEncoder(w).Encode(types.APIResponseData{Code: 200, Data: tags})
}

func (api *ApiHandler) getTagById(w http.ResponseWriter, r *http.Request) {

	idParam := mux.Vars(r)["id"]

	id, err := strconv.Atoi(idParam)

	if err != nil {
		api.writeError(&types.APIError{Err: err, Code: 400, Message: "failed to parse your id param to int. The param was: " + idParam}, w)
		return
	}

	tag, err := getTagById(id, api.DBPool)

	if err != nil {
		api.writeError(err, w)
		return
	}

	json.NewEncoder(w).Encode(types.APIResponseData{Code: 200, Data: tag})
}

func getTagById(id int, dbPool PgxPoolIface) (types.Tag, error) {

	ctx, cancel := context.WithTimeout(context.Background(), DEFAULT_QUERY_TIMEOUT)
	rows, err := dbPool.Query(ctx, "SELECT * FROM tag WHERE id=$1", id)
	defer cancel()

	if err != nil {
		return types.Tag{}, &types.APIError{Err: err, Code: 500, Message: "failed to query database"}
	}

	var tag types.Tag
	err = pgxscan.ScanOne(&tag, rows)

	if err != nil {
		var apiError error
		if errors.Is(err, pgx.ErrNoRows) {
			apiError = &types.APIError{Err: err, Code: 404, Message: "no tag with this id found"}
		} else {
			apiError = &types.APIError{Err: err, Code: 500, Message: "failed to scan result"}
		}

		return types.Tag{}, apiError
	}

	return tag, nil
}
