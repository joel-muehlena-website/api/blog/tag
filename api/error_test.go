package api

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
	"gitlab.com/joel-muehlena-website/api/blog/tag/types"
)

func TestError(t *testing.T) {

	config := types.Config{Port: 3005, DBConfig: types.DBConfig{
		Username: "testUser",
		Password: "password",
		Port:     5432,
		Host:     "testServer.de",
		Database: "blogDB",
	}}

	testApi := New(config)

	apiH := ApiHandler{Api: testApi}

	type testErrors struct {
		Err          error
		ExpectedCode int
	}

	tstErrors := []testErrors{
		{Err: &types.APIError{Code: 500, Message: "Unexpected error", Err: fmt.Errorf("There was an error. Please contact the admin.")}, ExpectedCode: 500},
		{Err: &types.APIError{Code: 404, Message: "Page not found", Err: fmt.Errorf("Endpoint not found.")}, ExpectedCode: 404},
		{Err: &types.APIError{Code: 403, Message: "Unauthorized", Err: fmt.Errorf("Please login first.")}, ExpectedCode: 403},
		{Err: &types.APIError{Code: 401, Message: "Unexpected error", Err: fmt.Errorf("There was an error. Please contact the admin.")}, ExpectedCode: 401},
	}

	t.Run("should send expected error back", func(t *testing.T) {

		for _, tstErr := range tstErrors {

			testApi.Router = mux.NewRouter().StrictSlash(true)
			testApi.Router.NewRoute().Path("/").Methods("GET").HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				apiH.writeError(tstErr.Err, w)
			})

			req, err := http.NewRequest("GET", "/", nil)
			assert.Nil(t, err)

			rr := httptest.NewRecorder()
			testApi.Router.ServeHTTP(rr, req)

			resp := rr.Result()
			assert.Equal(t, tstErr.ExpectedCode, resp.StatusCode)

			var respBody struct {
				Code   int      `json:"code"`
				Msg    string   `json:"msg"`
				Errors []string `json:"errors"`
			}

			json.NewDecoder(resp.Body).Decode(&respBody)
			assert.Equal(t, tstErr.ExpectedCode, respBody.Code)

			apiError, ok := tstErr.Err.(*types.APIError)
			assert.True(t, ok)

			assert.Equal(t, apiError.Message, respBody.Msg)
			assert.Equal(t, tstErr.Err.Error(), respBody.Errors[0])
		}
	})

}
