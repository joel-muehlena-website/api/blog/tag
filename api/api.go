package api

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
	"time"

	"github.com/fatih/color"
	"github.com/go-playground/validator/v10"
	"github.com/gorilla/mux"
	"github.com/jackc/pgconn"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/joel-muehlena-website/api/blog/tag/types"
)

const DEFAULT_QUERY_TIMEOUT time.Duration = time.Second * 3

//Print output colors
var (
	RED    = color.New(color.FgRed)
	GREEN  = color.New(color.FgGreen)
	YELLOW = color.New(color.FgYellow)
)

type PgxPoolIface interface {
	Query(context.Context, string, ...interface{}) (pgx.Rows, error)
	QueryRow(context.Context, string, ...interface{}) pgx.Row
	QueryFunc(ctx context.Context, sql string, args []interface{}, scans []interface{}, f func(pgx.QueryFuncRow) error) (pgconn.CommandTag, error)
	Acquire(ctx context.Context) (*pgxpool.Conn, error)
	Close()
}

type Api struct {
	Router *mux.Router
	Config types.Config
	DBPool PgxPoolIface
}

type ApiHandler struct {
	*Api
	Validator *validator.Validate
}

// Returns if the current run is a test run.
// This env var must be set by the test or the executing user
func isTest() bool {
	testEnvVar := os.Getenv("GO_ENV_JM")
	return testEnvVar == "test"
}

func New(config types.Config) (api *Api) {

	api = new(Api)

	if config.DBConfig.ConnectionTimeout == 0 {
		config.DBConfig.ConnectionTimeout = 5
	}

	if config.DBConfig.InitialConnectionRetry == 0 {
		config.DBConfig.InitialConnectionRetry = 3
	}

	api.Config = config
	api.Router = mux.NewRouter().StrictSlash(true)
	api.DBPool = nil

	handler := ApiHandler{api, validator.New()}

	for _, route := range handler.getRoutes() {
		api.Router.NewRoute().
			Name(route.Name).
			Methods(route.Method).
			Path(route.Path).
			HandlerFunc(route.HandlerFunc)
	}

	api.Router.NewRoute().Name("Healthz").Path("/healthz").Methods("GET").HandlerFunc(func(w http.ResponseWriter, r *http.Request) { w.WriteHeader(200) })

	if !isTest() {
		api.tryConnectToPostgres()
	}

	return
}

//sets the options for the pgxpool.Config
func (api *Api) configureDBPool(poolCfg *pgxpool.Config) error {
	poolCfg.MinConns = 2
	poolCfg.MaxConns = 5
	poolCfg.MaxConnLifetime = time.Minute * 15
	poolCfg.MaxConnIdleTime = time.Minute * 5

	if api.Config.DBConfig.LazyConnect {
		poolCfg.LazyConnect = true
	}

	if api.Config.DBConfig.CAPath != "" {
		tlsConfig, err := createTLSConfig(api.Config.DBConfig.CAPath)

		if err != nil {
			return err
		}

		poolCfg.ConnConfig.Config.TLSConfig = tlsConfig
	}

	poolCfg.AfterConnect = func(c1 context.Context, c2 *pgx.Conn) error {
		GREEN.Println("[DB LOG] Connected to db")
		return nil
	}

	poolCfg.BeforeConnect = func(c context.Context, cc *pgx.ConnConfig) error {
		YELLOW.Println("[DB LOG] Try to connect to db")
		return nil
	}

	return nil
}

//Logs an postgresql connection error with a custom message and exits the program
func dbConnErr(msg string, err error) {
	RED.Printf("[DB LOG] %s\n", msg)
	RED.Printf("[DB LOG] failed to connect to database: %v\n", err)
	RED.Printf("[DB LOG] pgconn.Timeout(err): %v\n", pgconn.Timeout(err))
	panic(err)
}

//Create a connection pool for the postgresql database
func (api *Api) tryConnectToPostgres() {
	poolCfg, err := pgxpool.ParseConfig(api.createPostgreSQLConnectionString())

	if err != nil {
		RED.Printf("[DB LOG] Failed to create database cfg: %v", err)
		panic(err)
	}

	err = api.configureDBPool(poolCfg)
	if err != nil {
		RED.Printf("[DB LOG] Failed to configure database: %v", err)
		panic(err)
	}

	connectionTimeout := time.Second * time.Duration(api.Config.DBConfig.ConnectionTimeout)
	connectionRetryCount := api.Config.DBConfig.InitialConnectionRetry

	var dbpool *pgxpool.Pool

	connected := false
	for i := 0; i < connectionRetryCount; i++ {

		fmt.Printf("[DB LOG] Init connection try %d\n", i+1)

		dbpool, err = connectToPostgreSQL(connectionTimeout, poolCfg)

		if err == nil {
			connected = true
			break
		}

		if i+1 == connectionRetryCount && !api.Config.DBConfig.LazyConnectFallback {
			dbConnErr("reached max tries exiting...", err)
		}

		fmt.Println("[DB LOG] Waiting 1 second before retrying")
		time.Sleep(time.Second * 1)
	}

	if !connected && api.Config.DBConfig.LazyConnectFallback {
		YELLOW.Printf("[DB LOG] Try connection in lazyConnect mode\n")
		poolCfg.LazyConnect = true
		dbpool, err = connectToPostgreSQL(connectionTimeout, poolCfg)

		if err != nil {
			dbConnErr("lazyConnect fallback also failed...", err)
		}
	}

	GREEN.Println("[DB LOG] Connection Successful")
	api.DBPool = dbpool
}

func connectToPostgreSQL(timeout time.Duration, cfg *pgxpool.Config) (*pgxpool.Pool, error) {
	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	dbpool, err := pgxpool.ConnectConfig(ctx, cfg)
	defer cancel()
	return dbpool, err
}

// create the connection string for the pgx driver
func (api *Api) createPostgreSQLConnectionString() string {
	connString := fmt.Sprintf(
		"user=%s password=%s host=%s",
		api.Config.DBConfig.Username,
		api.Config.DBConfig.Password,
		api.Config.DBConfig.Host,
	)

	if api.Config.DBConfig.Port != 0 {
		connString += " port=" + strconv.Itoa(int(api.Config.DBConfig.Port))
	}

	if api.Config.DBConfig.Database != "" {
		connString += " dbname=" + api.Config.DBConfig.Database
	}

	if api.Config.DBConfig.CAPath != "" {
		connString += " sslmode=verify-ca"
	}

	return connString
}

// loads the pem certificate from the given path and creates a tls.Config with it as CertPool
func createTLSConfig(certPath string) (*tls.Config, error) {
	CACert, err := ioutil.ReadFile(certPath)
	if err != nil {
		RED.Printf("[LOG] Failed to load server certificate: %v\n", err)
		return nil, err
	}

	CACertPool := x509.NewCertPool()
	CACertPool.AppendCertsFromPEM(CACert)

	tlsConfig := &tls.Config{
		RootCAs:            CACertPool,
		InsecureSkipVerify: true,
	}

	return tlsConfig, nil
}

func (api *Api) ListenAndServe() {
	GREEN.Printf("Server started on port: %d\n", api.Config.Port)
	http.ListenAndServe(":"+strconv.Itoa(int(api.Config.Port)), api.Router)
}
