package types

import (
	"time"
)

type Tag struct {
	Id        int       `db:"id" schema:"id"`
	Name      string    `db:"name" json:"name" validate:"required" schema:"name"`
	CreatedAt time.Time `db:"createdat" json:"createdAt" validate:"omitempty" schema:"createdAt"`
	UpdatedAt time.Time `db:"updatedat" json:"updatedAt" validate:"omitempty" schema:"updatedAt"`
}
