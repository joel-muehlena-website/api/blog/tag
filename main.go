package main

import (
	"os"

	"gitlab.com/joel-muehlena-website/api/blog/tag/api"
	"gitlab.com/joel-muehlena-website/api/blog/tag/types"
	configServer "gitlab.com/joel-muehlena-website/utility/jm-config-server-addon-go"
	"gopkg.in/yaml.v3"
)

func main() {

	configServerUrl := "http://localhost:8081/config"
	filepath := "blog/tag.dev.yaml"

	if os.Getenv("JM_SVC_ENV") == "production" {
		configServerUrl = "http://config-server.default.svc.cluster.local:8081/config"
		filepath = "blog/tag.prod.yaml"
	}

	resultChannel := make(chan configServer.Result, 1)
	configServer.FetchConfig(configServerUrl, resultChannel, configServer.WithFilePath(filepath), configServer.WithLogging(configServer.ERROR))

	result := <-resultChannel

	if result.Error != nil {
		println(result.Error.Error())
		os.Exit(1)
	}

	var config types.Config
	err := yaml.Unmarshal(result.Data, &config)

	if err != nil {
		println(err.Error())
		os.Exit(1)
	}

	config.DBConfig.LazyConnectFallback = true

	api := api.New(config)

	api.ListenAndServe()
}
