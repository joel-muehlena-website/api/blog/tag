package api

import (
	"bytes"
	"crypto/rand"
	"crypto/rsa"
	"crypto/tls"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/pem"
	"fmt"
	"io/ioutil"
	"math/big"
	"os"
	"strconv"
	"testing"
	"time"

	"github.com/go-test/deep"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/pashagolub/pgxmock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/joel-muehlena-website/api/blog/tag/types"
)

func TestMain(m *testing.M) {
	err := os.Setenv("GO_ENV_JM", "test")

	if err != nil {
		os.Exit(1)
	}

	os.Exit(m.Run())
}

func createDBMock(api *Api) (pgxmock.PgxPoolIface, error) {
	dbMockI, err := pgxmock.NewPool()

	if err != nil {
		return nil, err
	}

	api.DBPool = dbMockI
	return dbMockI, err
}

func TestNew(t *testing.T) {

	config := types.Config{Port: 3005, DBConfig: types.DBConfig{
		Username: "testUser",
		Password: "password",
		Port:     5432,
		Host:     "testServer.de",
		Database: "blogDB",
	}}

	dbMock, err := pgxmock.NewPool()
	assert.Nil(t, err)
	defer dbMock.Close()

	testApi := New(config)

	config.DBConfig.InitialConnectionRetry = 3
	config.DBConfig.ConnectionTimeout = 5

	assert.NotNil(t, testApi.Router)
	assert.Equal(t, config, testApi.Config)

	dbMock.ExpectBegin()

}

func TestConfigureDBPool(t *testing.T) {
	t.Run("load tls cert", func(t *testing.T) {
		t.Run("should configure the pool to not load a certificate on default", func(t *testing.T) {
			testApi := Api{Config: types.Config{}}
			poolCfg := pgxpool.Config{ConnConfig: &pgx.ConnConfig{}}
			testApi.configureDBPool(&poolCfg)
			assert.Nil(t, poolCfg.ConnConfig.TLSConfig)
		})

		t.Run("should configure the pool to load a certificate", func(t *testing.T) {

			caPEM := createTestCertificate(t)

			//Create a tmp dir
			dir, err := ioutil.TempDir("./", "pemData")
			assert.Nil(t, err)
			defer os.RemoveAll(dir)

			//Create a tmp file
			file, err := ioutil.TempFile(dir, "TestSQLCA.pem")
			assert.Nil(t, err)
			defer os.Remove(file.Name())
			fileName := file.Name()

			//write the test pem to that file
			_, err = file.Write(caPEM.Bytes())
			assert.Nil(t, err)
			file.Close()

			testApi := Api{Config: types.Config{DBConfig: types.DBConfig{CAPath: fileName}}}
			poolCfg := pgxpool.Config{ConnConfig: &pgx.ConnConfig{}}
			testApi.configureDBPool(&poolCfg)

			CACert, err := ioutil.ReadFile(fileName)
			assert.Nil(t, err)

			CACertPool := x509.NewCertPool()
			CACertPool.AppendCertsFromPEM(CACert)

			tlsConfig := &tls.Config{
				RootCAs:            CACertPool,
				InsecureSkipVerify: true,
			}

			assert.Nil(t, deep.Equal(*tlsConfig, *poolCfg.ConnConfig.TLSConfig))
		})

		t.Run("should return error for loading cert", func(t *testing.T) {
			//Create a tmp dir
			dir, err := ioutil.TempDir("./", "pemData")
			assert.Nil(t, err)
			defer os.RemoveAll(dir)

			testApi := Api{Config: types.Config{DBConfig: types.DBConfig{CAPath: dir + "/testFile.pem"}}}
			poolCfg := pgxpool.Config{ConnConfig: &pgx.ConnConfig{}}

			err = testApi.configureDBPool(&poolCfg)
			assert.NotNil(t, err)
		})
	})
}

func TestCreateConnectionString(t *testing.T) {

	cfg := types.Config{DBConfig: types.DBConfig{Username: "testUser", Password: "testPassword", Host: "testHost.com"}}

	t.Parallel()

	t.Run("should create string with just username password and host", func(t *testing.T) {
		testApi := Api{Config: cfg}
		s := testApi.createPostgreSQLConnectionString()

		assert.Contains(t, s, "user="+cfg.DBConfig.Username)
		assert.Contains(t, s, "password="+cfg.DBConfig.Password)
		assert.Contains(t, s, "host="+cfg.DBConfig.Host)
	})

	t.Run("should create string with just username password host and port", func(t *testing.T) {

		cfg := cfg
		cfg.DBConfig.Port = 2345

		testApi := Api{Config: cfg}
		s := testApi.createPostgreSQLConnectionString()

		assert.Contains(t, s, "user="+cfg.DBConfig.Username)
		assert.Contains(t, s, "password="+cfg.DBConfig.Password)
		assert.Contains(t, s, "host="+cfg.DBConfig.Host)
		assert.Contains(t, s, "port="+strconv.Itoa(int(cfg.DBConfig.Port)))
	})

	t.Run("should create string with just username password host and database", func(t *testing.T) {
		cfg := cfg
		cfg.DBConfig.Database = "testdb"

		testApi := Api{Config: cfg}
		s := testApi.createPostgreSQLConnectionString()

		assert.Contains(t, s, "user="+cfg.DBConfig.Username)
		assert.Contains(t, s, "password="+cfg.DBConfig.Password)
		assert.Contains(t, s, "host="+cfg.DBConfig.Host)
		assert.Contains(t, s, "dbname="+cfg.DBConfig.Database)
	})

	t.Run("should create string with just username password host and sslmode", func(t *testing.T) {
		cfg := cfg
		cfg.DBConfig.CAPath = "./testCA.pem"

		testApi := Api{Config: cfg}
		s := testApi.createPostgreSQLConnectionString()

		assert.Contains(t, s, "user="+cfg.DBConfig.Username)
		assert.Contains(t, s, "password="+cfg.DBConfig.Password)
		assert.Contains(t, s, "host="+cfg.DBConfig.Host)
		assert.Contains(t, s, "sslmode=verify-ca")
	})

	t.Run("should create string with all", func(t *testing.T) {

		cfg := cfg
		cfg.DBConfig.Port = 2345
		cfg.DBConfig.Database = "testdb"
		cfg.DBConfig.CAPath = "./testCA.pem"

		testApi := Api{Config: cfg}
		s := testApi.createPostgreSQLConnectionString()

		assert.Contains(t, s, "user="+cfg.DBConfig.Username)
		assert.Contains(t, s, "password="+cfg.DBConfig.Password)
		assert.Contains(t, s, "host="+cfg.DBConfig.Host)
		assert.Contains(t, s, "port="+strconv.Itoa(int(cfg.DBConfig.Port)))
		assert.Contains(t, s, "dbname="+cfg.DBConfig.Database)
		assert.Contains(t, s, "sslmode=verify-ca")
	})
}

func TestIsTest(t *testing.T) {
	val := os.Getenv("GO_ENV_JM")

	os.Setenv("GO_ENV_JM", "test")
	assert.True(t, isTest())
	os.Setenv("GO_ENV_JM", "")
	assert.False(t, isTest())
	os.Setenv("GO_ENV_JM", val)
}

func TestDBConnErr(t *testing.T) {
	t.Run("should panic on called", func(t *testing.T) {
		assert.Panics(t, func() { dbConnErr("Test Error", fmt.Errorf("An error occured")) })
	})
}

func TestDBConnection(t *testing.T) {

	t.Run("should connect to postgres", func(t *testing.T) {
		if os.Getenv("POSTGRES_TEST_ENABLED") != "true" {
			t.Skip("Postgres testing not enabled. Please set the env var POSTGRES_TEST_ENABLED to `true`. You need also to set the postgres driver env variables:\n\t- PGHOST\n\t- PGPORT\n\t- PGDATABASE\n\t- PGUSER\n")
			return
		}
		testApi := New(types.Config{})
		assert.NotPanics(t, func() { testApi.tryConnectToPostgres() })
	})

	t.Run("should fail connect to postgres", func(t *testing.T) {
		testApi := New(types.Config{})
		assert.Panics(t, func() { testApi.tryConnectToPostgres() })
	})

	t.Run("should create pool if lazy connect is enabled", func(t *testing.T) {
		testApi := New(types.Config{})
		testApi.Config.DBConfig.LazyConnect = true
		assert.NotPanics(t, func() { testApi.tryConnectToPostgres() })
	})

	t.Run("should create pool if lazy connect fallback is enabled", func(t *testing.T) {
		testApi := New(types.Config{})
		testApi.Config.DBConfig.LazyConnectFallback = true
		assert.NotPanics(t, func() { testApi.tryConnectToPostgres() })
	})

}

func createTestCertificate(t *testing.T) *bytes.Buffer {
	ca := &x509.Certificate{
		SerialNumber: big.NewInt(2019),
		Subject: pkix.Name{
			Organization:  []string{"Company, INC."},
			Country:       []string{"US"},
			Province:      []string{""},
			Locality:      []string{"San Francisco"},
			StreetAddress: []string{"Golden Gate Bridge"},
			PostalCode:    []string{"94016"},
		},
		NotBefore:             time.Now(),
		NotAfter:              time.Now().AddDate(10, 0, 0),
		IsCA:                  true,
		ExtKeyUsage:           []x509.ExtKeyUsage{x509.ExtKeyUsageClientAuth, x509.ExtKeyUsageServerAuth},
		KeyUsage:              x509.KeyUsageDigitalSignature | x509.KeyUsageCertSign,
		BasicConstraintsValid: true,
	}

	caPrivKey, err := rsa.GenerateKey(rand.Reader, 4096)
	assert.Nil(t, err)

	caBytes, err := x509.CreateCertificate(rand.Reader, ca, ca, &caPrivKey.PublicKey, caPrivKey)
	assert.Nil(t, err)

	caPEM := new(bytes.Buffer)
	pem.Encode(caPEM, &pem.Block{
		Type:  "CERTIFICATE",
		Bytes: caBytes,
	})

	return caPEM
}
