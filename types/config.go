package types

type Config struct {
	Port     uint16   `yaml:"port"`
	DBConfig DBConfig `yaml:"dbConfig"`
}

type DBConfig struct {
	Username               string `yaml:"user"`
	Password               string `yaml:"password"`
	Port                   uint16 `yaml:"port"`
	Host                   string `yaml:"host"`
	Database               string `yaml:"database"`
	CAPath                 string `yaml:"caPath"`
	ConnectionTimeout      int    `yaml:"connectionTimeout"`
	InitialConnectionRetry int    `yaml:"initConnRetry"`
	LazyConnect            bool   `yaml:"lazyConnect"`
	LazyConnectFallback    bool   `yaml:"lazyConnectFallback"`
}
