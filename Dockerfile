FROM alpine

ENV GOOS=linux

WORKDIR /dist

COPY  ./main ./main

ENV JM_SVC_ENV="production"

EXPOSE 80

CMD ["./main"]
