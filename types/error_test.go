package types

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestApiError(t *testing.T) {

	errorText := "Test Error"
	errorMessage := "Test MSG"
	errorr := fmt.Errorf(errorText)

	err := APIError{Err: errorr, Code: 400, Message: errorMessage}

	assert.Equal(t, err.Error(), fmt.Sprintf("An API error occured with code %d and the msg: %s and the text: %s", err.Code, err.Message, errorr.Error()))
}
