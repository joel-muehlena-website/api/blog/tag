package types

import "net/http"

type RouteList []Route

type Route struct {
	Name        string
	Path        string
	Method      string
	HandlerFunc http.HandlerFunc
}

type APIResponseMsg struct {
	Code uint16 `json:"code"`
	Msg  string `json:"msg"`
}

type APIResponseData struct {
	Code uint16      `json:"code"`
	Data interface{} `json:"data"`
}
