package api

import (
	"bytes"
	"encoding/json"
	"errors"
	"net/http"
	"net/http/httptest"
	"strconv"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/joel-muehlena-website/api/blog/tag/types"
)

func TestUpdateTagbyId(t *testing.T) {
	testApi := New(types.Config{})

	t.Run("should return error 400 if no name is provided", func(t *testing.T) {
		testId := 6
		req, err := http.NewRequest("PATCH", "/tag/"+strconv.Itoa(testId), nil)
		assert.Nil(t, err)

		rr := httptest.NewRecorder()
		testApi.Router.ServeHTTP(rr, req)

		resp := rr.Result()

		assert.Equal(t, 400, resp.StatusCode)
	})

	t.Run("should update an db entry", func(t *testing.T) {

		type testData struct {
			OldName string
			NewName string
			Id      int
		}

		dbMock, err := createDBMock(testApi)
		assert.Nil(t, err)

		testDataList := []testData{
			{OldName: "testName", NewName: "a new name", Id: 2},
			{OldName: "school", NewName: "middle school", Id: 3},
		}

		for _, data := range testDataList {

			body, _ := json.Marshal(map[string]string{"name": data.NewName})
			req, err := http.NewRequest("PATCH", "/tag/"+strconv.Itoa(data.Id), bytes.NewBuffer(body))
			assert.Nil(t, err)
			req.Header.Add("Content-Type", "application/json")

			dbMock.ExpectQuery("SELECT [*] FROM tag WHERE id=").WithArgs(data.Id).WillReturnRows(dbMock.NewRows([]string{"id", "name", "createdat", "updatedat"}).AddRow(data.Id, data.OldName, time.Now(), time.Now()))
			dbMock.ExpectQuery("UPDATE tag SET name=(.+?), updatedat=(.+?) WHERE id=(.+?) RETURNING id").WithArgs(data.NewName, AnyTime(), data.Id).WillReturnRows(dbMock.NewRows([]string{"id"}).AddRow(data.Id))

			rr := httptest.NewRecorder()
			testApi.Router.ServeHTTP(rr, req)

			resp := rr.Result()

			var respBody struct {
				Code int       `json:"code"`
				Data types.Tag `json:"data"`
			}

			json.NewDecoder(resp.Body).Decode(&respBody)

			assert.Equal(t, 200, resp.StatusCode)
			assert.Equal(t, 200, respBody.Code)
			assert.Equal(t, data.Id, respBody.Data.Id)
			assert.Equal(t, data.NewName, respBody.Data.Name)
			assert.Nil(t, dbMock.ExpectationsWereMet())
		}
	})

	t.Run("should fail query and return 500", func(t *testing.T) {

		dbMock, err := createDBMock(testApi)
		assert.Nil(t, err)

		testId := 6
		body, _ := json.Marshal(map[string]string{"name": "test"})
		req, err := http.NewRequest("PATCH", "/tag/"+strconv.Itoa(testId), bytes.NewBuffer(body))
		assert.Nil(t, err)
		req.Header.Add("Content-Type", "application/json")

		dbMock.ExpectQuery("SELECT [*] FROM tag WHERE id=").WithArgs(testId).WillReturnRows(dbMock.NewRows([]string{"id", "name", "createdat", "updatedat"}).AddRow(testId, "test", time.Now(), time.Now()))
		dbMock.ExpectQuery("UPDATE tag SET name=(.+?), updatedat=(.+?) WHERE id=(.+?) RETURNING id").WithArgs("test", AnyTime(), testId).WillReturnError(errors.New("db mock test error"))

		rr := httptest.NewRecorder()
		testApi.Router.ServeHTTP(rr, req)

		resp := rr.Result()

		assert.Equal(t, 500, resp.StatusCode)
		assert.Nil(t, dbMock.ExpectationsWereMet())
	})

	t.Run("should return 400 error if param is no int", func(t *testing.T) {
		testDataList := []string{"will_not_work", "noInt"}

		for _, tParam := range testDataList {
			req, _ := http.NewRequest("PATCH", "/tag/"+tParam, nil)

			recc := httptest.NewRecorder()
			testApi.Router.ServeHTTP(recc, req)

			res := recc.Result()

			assert.Equal(t, 400, res.StatusCode)
		}
	})

	t.Run("should return 404 error if id is not found", func(t *testing.T) {
		testDataList := []int{1, 2}

		dbMock, err := createDBMock(testApi)
		assert.Nil(t, err)

		for _, tId := range testDataList {
			body, _ := json.Marshal(map[string]string{"name": "noop"})
			req, _ := http.NewRequest("PATCH", "/tag/"+strconv.Itoa(tId), bytes.NewBuffer(body))
			req.Header.Add("Content-Type", "application/json")

			dbMock.ExpectQuery("SELECT [*] FROM tag WHERE id=(.+?)$").WithArgs(tId).WillReturnRows(dbMock.NewRows([]string{"id", "name", "createdat", "updatedat"}))

			recc := httptest.NewRecorder()
			testApi.Router.ServeHTTP(recc, req)

			res := recc.Result()

			assert.Equal(t, 404, res.StatusCode)
			assert.Nil(t, dbMock.ExpectationsWereMet())
		}
	})
}
