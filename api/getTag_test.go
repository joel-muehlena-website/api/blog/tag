package api

import (
	"encoding/json"
	"errors"
	"net/http"
	"net/http/httptest"
	"net/url"
	"strconv"
	"strings"
	"testing"
	"time"

	"github.com/go-test/deep"
	"github.com/jackc/pgtype"
	"github.com/pashagolub/pgxmock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/joel-muehlena-website/api/blog/tag/types"
)

func WithInt4ArrayArg(arr *pgtype.Int4Array) pgxmock.Argument {
	return withInt4ArrayArg{expected: arr}
}

type withInt4ArrayArg struct {
	expected *pgtype.Int4Array
}

func (arr withInt4ArrayArg) Match(v interface{}) bool {
	diff := deep.Equal(arr.expected, v)
	return diff == nil
}

func TestGetTag(t *testing.T) {
	testApi := New(types.Config{})

	t.Run("should fail query and return 500", func(t *testing.T) {
		req, err := http.NewRequest("GET", "/tag", nil)
		assert.Nil(t, err)

		dbMock, err := createDBMock(testApi)
		assert.Nil(t, err)

		dbMock.ExpectQuery("^SELECT [*] FROM tag").WillReturnError(errors.New("db mock test error"))

		rr := httptest.NewRecorder()
		testApi.Router.ServeHTTP(rr, req)

		resp := rr.Result()

		assert.Equal(t, 500, resp.StatusCode)
		assert.Nil(t, dbMock.ExpectationsWereMet())
	})

	t.Run("should return empty list if nothing is found", func(t *testing.T) {

		dbMock, err := createDBMock(testApi)
		assert.Nil(t, err)

		dbMock.ExpectQuery("^SELECT [*] FROM tag WHERE name LIKE").WithArgs("%" + "" + "%").WillReturnRows(dbMock.NewRows([]string{"id", "name", "createdat", "updatedat"}))

		req, err := http.NewRequest("GET", "/tag", nil)
		assert.Nil(t, err)

		rr := httptest.NewRecorder()
		testApi.Router.ServeHTTP(rr, req)

		resp := rr.Result()
		assert.Equal(t, 200, resp.StatusCode)

		var respBody struct {
			Code int         `json:"code"`
			Data []types.Tag `json:"data"`
		}

		json.NewDecoder(resp.Body).Decode(&respBody)

		assert.Equal(t, make([]types.Tag, 0), respBody.Data)
		assert.Equal(t, 200, respBody.Code)
		assert.Nil(t, dbMock.ExpectationsWereMet())
	})

	t.Run("should return right data if something is found", func(t *testing.T) {

		testData := []types.Tag{
			{Id: 1, Name: "test tag", CreatedAt: time.Now(), UpdatedAt: time.Now()},
			{Id: 2, Name: "another", CreatedAt: time.Date(2022, 1, 10, 12, 1, 23, 34, time.Local), UpdatedAt: time.Now()},
		}

		dbMock, err := createDBMock(testApi)
		assert.Nil(t, err)

		mockRows := dbMock.NewRows([]string{"id", "name", "createdat", "updatedat"})

		for _, td := range testData {
			mockRows.AddRow(td.Id, td.Name, td.CreatedAt, td.UpdatedAt)
		}

		dbMock.ExpectQuery("^SELECT [*] FROM tag WHERE name LIKE").WithArgs("%" + "" + "%").WillReturnRows(mockRows)

		req, err := http.NewRequest("GET", "/tag", nil)
		assert.Nil(t, err)

		rr := httptest.NewRecorder()
		testApi.Router.ServeHTTP(rr, req)

		resp := rr.Result()

		var respBody struct {
			Code int         `json:"code"`
			Data []types.Tag `json:"data"`
		}

		json.NewDecoder(resp.Body).Decode(&respBody)

		assert.Equal(t, 200, resp.StatusCode)
		assert.Equal(t, 200, respBody.Code)
		assert.Nil(t, deep.Equal(testData, respBody.Data))
		assert.Nil(t, dbMock.ExpectationsWereMet())
	})

	t.Run("should return list containing tag with right name by query pattern matching", func(t *testing.T) {

		dbMock, err := createDBMock(testApi)
		assert.Nil(t, err)

		filterTestData := func(data []types.Tag, query string) (filteredData []types.Tag) {
			filteredData = make([]types.Tag, 0)

			for _, entry := range data {
				if strings.Contains(entry.Name, query) {
					filteredData = append(filteredData, entry)
				}
			}

			return
		}

		testData := []types.Tag{
			{Id: 1, Name: "test tag"},
			{Id: 2, Name: "another"},
			{Id: 3, Name: "test tag 3"},
			{Id: 4, Name: "test_tag"},
			{Id: 5, Name: "tag5"},
			{Id: 6, Name: "5tag"},
			{Id: 7, Name: "something else"},
			{Id: 8, Name: "something"},
			{Id: 9, Name: "school"},
		}

		testQueries := []string{"tag", "5", "something", "encoded/query"}

		for _, query := range testQueries {

			mockRows := dbMock.NewRows([]string{"id", "name", "createdat", "updatedat"})

			filteredTestData := filterTestData(testData, query)

			for _, td := range filteredTestData {
				mockRows.AddRow(td.Id, td.Name, td.CreatedAt, td.UpdatedAt)
			}

			dbMock.ExpectQuery("^SELECT [*] FROM tag WHERE name LIKE").WithArgs("%" + query + "%").WillReturnRows(mockRows)

			req, err := http.NewRequest("GET", "/tag?name="+url.QueryEscape(query), nil)
			assert.Nil(t, err)

			rr := httptest.NewRecorder()
			testApi.Router.ServeHTTP(rr, req)

			resp := rr.Result()

			var respBody struct {
				Code int         `json:"code"`
				Data []types.Tag `json:"data"`
			}

			json.NewDecoder(resp.Body).Decode(&respBody)

			assert.Equal(t, 200, resp.StatusCode)
			assert.Nil(t, deep.Equal(filteredTestData, respBody.Data))
			assert.Nil(t, dbMock.ExpectationsWereMet())
		}

	})

	t.Run("should return a list of tags by multiple ids", func(t *testing.T) {

		filterData := func(data []types.Tag, ids []int) (tags []types.Tag) {
			tags = make([]types.Tag, 0)

			for _, d := range data {

				for _, id := range ids {
					if id == d.Id {
						tags = append(tags, d)
					}
				}
			}

			return
		}

		testDataList := []types.Tag{
			{Id: 1, Name: "test tag"},
			{Id: 2, Name: "another"},
			{Id: 3, Name: "test tag 3"},
			{Id: 4, Name: "test_tag"},
			{Id: 5, Name: "tag5"},
			{Id: 6, Name: "5tag"},
			{Id: 7, Name: "something else"},
			{Id: 8, Name: "something"},
			{Id: 9, Name: "school"},
		}

		testQueries := [][]int{{2, 4, 5}, {}, {1, 3}, {1}}

		dbMock, err := createDBMock(testApi)
		assert.Nil(t, err)

		for _, query := range testQueries {
			mockRows := dbMock.NewRows([]string{"id", "name", "createdat", "updatedat"})

			var filteredTestData []types.Tag = testDataList

			if len(query) > 0 {
				filteredTestData = filterData(testDataList, query)
			}

			for _, fData := range filteredTestData {
				mockRows.AddRow(fData.Id, fData.Name, fData.CreatedAt, fData.UpdatedAt)
			}

			req, err := http.NewRequest("GET", "/tag", nil)
			assert.Nil(t, err)

			oldQuery := req.URL.Query()

			for _, queryVal := range query {
				oldQuery.Add("id[]", url.QueryEscape(strconv.Itoa(queryVal)))
			}

			req.URL.RawQuery = oldQuery.Encode()

			if len(query) == 0 {
				dbMock.ExpectQuery("^SELECT [*] FROM tag WHERE name LIKE").WithArgs("%" + "" + "%").WillReturnRows(mockRows)
			} else {
				arr := &pgtype.Int4Array{}
				arr.Set(query)
				dbMock.ExpectQuery("^SELECT [*] FROM tag WHERE id=ANY").WithArgs(WithInt4ArrayArg(arr)).WillReturnRows(mockRows)
			}

			rr := httptest.NewRecorder()
			testApi.Router.ServeHTTP(rr, req)

			resp := rr.Result()

			var respBody struct {
				Code int         `json:"code"`
				Data []types.Tag `json:"data"`
			}

			json.NewDecoder(resp.Body).Decode(&respBody)

			assert.Equal(t, 200, resp.StatusCode)
			assert.Nil(t, deep.Equal(filteredTestData, respBody.Data))
			assert.Nil(t, dbMock.ExpectationsWereMet())
		}
	})

	t.Run("should return an 400 error if name and id[] query is set", func(t *testing.T) {

		type testData struct {
			Name string
			Ids  []int
		}

		testQueries := []testData{{Name: "test", Ids: []int{2, 34, 2}}, {Name: "another tag", Ids: []int{4}}}

		for _, query := range testQueries {

			req, err := http.NewRequest("GET", "/tag", nil)
			assert.Nil(t, err)

			oldQuery := req.URL.Query()

			for _, queryVal := range query.Ids {
				oldQuery.Add("id[]", url.QueryEscape(strconv.Itoa(queryVal)))
			}

			oldQuery.Add("name", url.QueryEscape(query.Name))
			req.URL.RawQuery = oldQuery.Encode()

			rr := httptest.NewRecorder()
			testApi.Router.ServeHTTP(rr, req)

			resp := rr.Result()
			assert.Equal(t, 400, resp.StatusCode)

		}
	})
}

func TestGetTagById(t *testing.T) {
	testApi := New(types.Config{})

	t.Run("should return a single tag by id", func(t *testing.T) {
		type testData struct {
			Id   int
			Name string
		}

		testDataList := []testData{{Id: 1, Name: "testTag"}, {Id: 2, Name: "tag2"}}

		dbMock, err := createDBMock(testApi)
		assert.Nil(t, err)

		for _, data := range testDataList {
			req, _ := http.NewRequest("GET", "/tag/"+strconv.Itoa(data.Id), nil)

			dbMock.ExpectQuery("^SELECT [*] FROM tag WHERE id=").WithArgs(data.Id).WillReturnRows(dbMock.NewRows([]string{"id", "name", "createdat", "updatedat"}).AddRow(data.Id, data.Name, time.Now(), time.Now()))

			recc := httptest.NewRecorder()
			testApi.Router.ServeHTTP(recc, req)

			res := recc.Result()

			assert.Equal(t, 200, res.StatusCode)
			assert.Nil(t, dbMock.ExpectationsWereMet())
		}
	})

	t.Run("should return 400 error if param is no int", func(t *testing.T) {
		testDataList := []string{"will_not_work", "noInt"}

		for _, tParam := range testDataList {
			req, _ := http.NewRequest("GET", "/tag/"+tParam, nil)

			recc := httptest.NewRecorder()
			testApi.Router.ServeHTTP(recc, req)

			res := recc.Result()

			assert.Equal(t, 400, res.StatusCode)
		}
	})

	t.Run("should return 404 error if id is not found", func(t *testing.T) {
		testDataList := []int{1, 2}

		dbMock, err := createDBMock(testApi)
		assert.Nil(t, err)

		for _, tId := range testDataList {
			req, _ := http.NewRequest("GET", "/tag/"+strconv.Itoa(tId), nil)

			dbMock.ExpectQuery("^SELECT [*] FROM tag WHERE id=").WithArgs(tId).WillReturnRows(dbMock.NewRows([]string{"id", "name", "createdat", "updatedat"}))

			recc := httptest.NewRecorder()
			testApi.Router.ServeHTTP(recc, req)

			res := recc.Result()

			assert.Equal(t, 404, res.StatusCode)
			assert.Nil(t, dbMock.ExpectationsWereMet())
		}
	})
}
