package api

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"
	"time"

	"github.com/jackc/pgconn"
	"github.com/pashagolub/pgxmock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/joel-muehlena-website/api/blog/tag/types"
)

func AnyTime() pgxmock.Argument {
	return anyTime{}
}

type anyTime struct{}

func (a anyTime) Match(v interface{}) bool {
	_, ok := v.(time.Time)
	return ok
}

func TestCreateTag(t *testing.T) {

	type testData struct {
		ContentType string
		Name        string
		ExpectedId  int
	}

	testApi := New(types.Config{})

	const (
		JSON_CONTENT     string = "application/json"
		WWW_FORM_CONTENT string = "application/x-www-form-urlencoded"
	)

	testDataList := []testData{
		{ContentType: JSON_CONTENT, Name: "testTag", ExpectedId: 1},
		{ContentType: WWW_FORM_CONTENT, Name: "testTag2", ExpectedId: 2},
	}

	t.Run("should create and insert new tag", func(t *testing.T) {
		for _, data := range testDataList {

			var req *http.Request

			if data.ContentType == JSON_CONTENT {

				body, _ := json.Marshal(map[string]string{"name": data.Name})
				req, _ = http.NewRequest("POST", "/tag", bytes.NewBuffer(body))
			} else if data.ContentType == WWW_FORM_CONTENT {

				formData := url.Values{
					"name": {data.Name},
				}

				req, _ = http.NewRequest("POST", "/tag", bytes.NewBufferString(formData.Encode()))
			}
			req.Header.Set("Content-Type", data.ContentType)

			dbMock, err := createDBMock(testApi)
			assert.Nil(t, err)

			dbMock.ExpectQuery("INSERT INTO tag").
				WithArgs(data.Name, AnyTime(), AnyTime()).
				WillReturnRows(dbMock.NewRows([]string{"id"}).AddRow(data.ExpectedId))

			recc := httptest.NewRecorder()

			testApi.Router.ServeHTTP(recc, req)

			res := recc.Result()

			var respBody struct {
				Code int       `json:"code"`
				Data types.Tag `json:"data"`
			}

			json.NewDecoder(res.Body).Decode(&respBody)

			assert.Equal(t, 200, res.StatusCode)
			assert.Equal(t, 200, respBody.Code)
			assert.Equal(t, data.ExpectedId, respBody.Data.Id)
			assert.Nil(t, dbMock.ExpectationsWereMet())
		}
	})

	t.Run("should create duplictae name error", func(t *testing.T) {
		for _, data := range testDataList {

			var req *http.Request

			if data.ContentType == JSON_CONTENT {

				body, _ := json.Marshal(map[string]string{"name": data.Name})
				req, _ = http.NewRequest("POST", "/tag", bytes.NewBuffer(body))
			} else if data.ContentType == WWW_FORM_CONTENT {

				formData := url.Values{
					"name": {data.Name},
				}

				req, _ = http.NewRequest("POST", "/tag", bytes.NewBufferString(formData.Encode()))
			}
			req.Header.Set("Content-Type", data.ContentType)

			dbMock, err := createDBMock(testApi)
			assert.Nil(t, err)

			dbMock.ExpectQuery("INSERT INTO tag").
				WithArgs(data.Name, AnyTime(), AnyTime()).
				WillReturnError(&pgconn.PgError{Message: "duplicate key value violates unique constraint \"tag_name_key\"", Code: "23505"})

			recc := httptest.NewRecorder()

			testApi.Router.ServeHTTP(recc, req)

			res := recc.Result()

			var respBody types.HTTPError

			json.NewDecoder(res.Body).Decode(&respBody)

			assert.Equal(t, 500, res.StatusCode)
			assert.Equal(t, 500, int(respBody.Code))
			assert.Equal(t, "duplicate key value violates unique constraint \"tag_name_key\"", respBody.Msg)
			assert.Nil(t, dbMock.ExpectationsWereMet())
		}
	})

}
