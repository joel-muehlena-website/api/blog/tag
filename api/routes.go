package api

import "gitlab.com/joel-muehlena-website/api/blog/tag/types"

func (api *ApiHandler) getRoutes() (routes types.RouteList) {

	routes = types.RouteList{
		types.Route{Name: "GetTagsList", Path: "/tag", Method: "GET", HandlerFunc: api.getTagList},
		types.Route{Name: "CreateTag", Path: "/tag", Method: "POST", HandlerFunc: api.createTag},
		types.Route{Name: "GetTagById", Path: "/tag/{id}", Method: "GET", HandlerFunc: api.getTagById},
		types.Route{Name: "DeleteTagById", Path: "/tag/{id}", Method: "DELETE", HandlerFunc: api.deleteTagById},
		types.Route{Name: "UpdateTagbyId", Path: "/tag/{id}", Method: "PATCH", HandlerFunc: api.updateTagbyId},
	}

	return
}
