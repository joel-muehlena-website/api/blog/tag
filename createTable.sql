DROP TABLE IF EXISTS tag;

CREATE TABLE tag (
  id SERIAL PRIMARY KEY,
  name varchar(255) NOT NULL,
  createdat timestamp,
  updatedat timestamp DEFAULT now(),
  UNIQUE(name)
);