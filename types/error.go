package types

import "fmt"

type HTTPError struct {
	Code   uint16   `json:"code"`
	Msg    string   `json:"msg"`
	Errors []string `json:"errors"`
}

type APIError struct {
	Err     error
	Code    uint16
	Message string
}

func (err *APIError) Error() string {
	return fmt.Sprintf("An API error occured with code %d and the msg: %s and the text: %s", err.Code, err.Message, err.Err.Error())
}
