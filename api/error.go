package api

import (
	"encoding/json"
	"net/http"

	"gitlab.com/joel-muehlena-website/api/blog/tag/types"
)

func (api *ApiHandler) writeError(err error, rw http.ResponseWriter) {

	var sCode uint16 = 500
	message := ""
	errMsg := err.Error()

	httpError := types.HTTPError{}

	if val, ok := err.(*types.APIError); ok {
		sCode = val.Code
		message = val.Message
	}

	httpError.Code = sCode
	httpError.Msg = message
	httpError.Errors = append(httpError.Errors, errMsg)

	rw.WriteHeader(int(sCode))
	json.NewEncoder(rw).Encode(httpError)
}
