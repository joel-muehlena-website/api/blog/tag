package api

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"time"

	"github.com/jackc/pgconn"
	"gitlab.com/joel-muehlena-website/api/blog/tag/types"
)

func (api *ApiHandler) createTag(w http.ResponseWriter, r *http.Request) {

	var tag types.Tag

	err := api.parseBody(r.Header.Get("Content-Type"), r, &tag)

	if err != nil {
		api.writeError(&types.APIError{Code: 400, Err: err, Message: "Wrong data received"}, w)
		return
	}

	err = api.Validator.Struct(tag)

	if err != nil {
		api.writeError(&types.APIError{Code: 400, Err: err, Message: "Wrong data received"}, w)
		return
	}

	tNow := time.Now()
	tag.UpdatedAt = tNow
	tag.CreatedAt = tNow

	ctx, cancel := context.WithTimeout(context.Background(), DEFAULT_QUERY_TIMEOUT)
	defer cancel()

	id := -1
	err = api.DBPool.QueryRow(ctx, "INSERT INTO tag (name, createdat, updatedat) VALUES ($1, $2, $3) RETURNING id", tag.Name, tag.CreatedAt, tag.UpdatedAt).Scan(&id)

	if err != nil {
		var pgErr *pgconn.PgError
		if errors.As(err, &pgErr) {
			api.writeError(&types.APIError{Code: 500, Err: fmt.Errorf("%s with code %s", pgErr.Message, pgErr.Code), Message: pgErr.Message}, w)
		} else {
			api.writeError(&types.APIError{Code: 500, Err: err, Message: "Error query database"}, w)
		}
		return
	}

	tag.Id = id

	if id != -1 {
		json.NewEncoder(w).Encode(types.APIResponseData{Code: 200, Data: tag})
	} else {
		api.writeError(&types.APIError{Code: 500, Err: fmt.Errorf("failed to fetch the id of the newly created tag maybe an error occured"), Message: "Failed to fetch id"}, w)
	}
}
