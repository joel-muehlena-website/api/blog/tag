package api

import (
	"encoding/json"
	"errors"
	"net/http"
	"net/http/httptest"
	"strconv"
	"testing"
	"time"

	"github.com/go-test/deep"
	"github.com/stretchr/testify/assert"
	"gitlab.com/joel-muehlena-website/api/blog/tag/types"
)

func TestDeleteTagById(t *testing.T) {
	testApi := New(types.Config{})

	t.Run("should fail query and return 500", func(t *testing.T) {
		testId := 6
		req, err := http.NewRequest("DELETE", "/tag/"+strconv.Itoa(testId), nil)
		assert.Nil(t, err)

		dbMock, err := createDBMock(testApi)
		assert.Nil(t, err)

		dbMock.ExpectQuery("SELECT [*] FROM tag WHERE id=").WithArgs(testId).WillReturnRows(dbMock.NewRows([]string{"id", "name", "createdat", "updatedat"}).AddRow(testId, "test", time.Now(), time.Now()))
		dbMock.ExpectQuery("DELETE FROM tag WHERE id=").WithArgs(testId).WillReturnError(errors.New("db mock test error"))

		rr := httptest.NewRecorder()
		testApi.Router.ServeHTTP(rr, req)

		resp := rr.Result()

		assert.Equal(t, 500, resp.StatusCode)
		assert.Nil(t, dbMock.ExpectationsWereMet())
	})

	t.Run("should delete a tag by id and return it", func(t *testing.T) {
		testDataList := []types.Tag{
			{Id: 1, Name: "test tag"},
			{Id: 2, Name: "another"},
			{Id: 3, Name: "test tag 3"},
			{Id: 4, Name: "test_tag"},
			{Id: 5, Name: "tag5"},
			{Id: 6, Name: "5tag"},
			{Id: 7, Name: "something else"},
			{Id: 8, Name: "something"},
			{Id: 9, Name: "school"},
		}

		for _, data := range testDataList {
			req, _ := http.NewRequest("DELETE", "/tag/"+strconv.Itoa(data.Id), nil)

			dbMock, err := createDBMock(testApi)
			assert.Nil(t, err)

			dbMock.ExpectQuery("^SELECT [*] FROM tag WHERE id=").WithArgs(data.Id).WillReturnRows(dbMock.NewRows([]string{"id", "name", "createdat", "updatedat"}).AddRow(data.Id, data.Name, data.CreatedAt, data.UpdatedAt))
			dbMock.ExpectQuery("DELETE FROM tag WHERE id=").WithArgs(data.Id).WillReturnRows(dbMock.NewRows([]string{"id"}).AddRow(data.Id))

			recc := httptest.NewRecorder()
			testApi.Router.ServeHTTP(recc, req)

			res := recc.Result()

			assert.Equal(t, 200, res.StatusCode)

			var respBody struct {
				Code int       `json:"code"`
				Data types.Tag `json:"data"`
			}

			json.NewDecoder(res.Body).Decode(&respBody)
			assert.Equal(t, 200, respBody.Code)
			assert.Nil(t, deep.Equal(data, respBody.Data))
			assert.Nil(t, dbMock.ExpectationsWereMet())
		}
	})

	t.Run("should return 400 error if param is no int", func(t *testing.T) {
		testDataList := []string{"will_not_work", "noInt"}

		for _, tParam := range testDataList {
			req, _ := http.NewRequest("GET", "/tag/"+tParam, nil)

			recc := httptest.NewRecorder()
			testApi.Router.ServeHTTP(recc, req)

			res := recc.Result()

			assert.Equal(t, 400, res.StatusCode)
		}
	})

	t.Run("should return 404 error if id is not found", func(t *testing.T) {
		testDataList := []int{1, 2}

		for _, tId := range testDataList {
			req, _ := http.NewRequest("GET", "/tag/"+strconv.Itoa(tId), nil)

			dbMock, err := createDBMock(testApi)
			assert.Nil(t, err)

			dbMock.ExpectQuery("^SELECT [*] FROM tag WHERE id=").WithArgs(tId).WillReturnRows(dbMock.NewRows([]string{"id", "name", "createdat", "updatedat"}))

			recc := httptest.NewRecorder()
			testApi.Router.ServeHTTP(recc, req)

			res := recc.Result()

			assert.Equal(t, 404, res.StatusCode)
			assert.Nil(t, dbMock.ExpectationsWereMet())
		}
	})
}
