package api

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"github.com/jackc/pgconn"
	"gitlab.com/joel-muehlena-website/api/blog/tag/types"
)

func (api *ApiHandler) deleteTagById(w http.ResponseWriter, r *http.Request) {

	idParam := mux.Vars(r)["id"]

	id, err := strconv.Atoi(idParam)

	if err != nil {
		api.writeError(&types.APIError{Err: err, Code: 400, Message: "failed to parse your id param to int. The param was: " + idParam}, w)
		return
	}

	tag, err := getTagById(id, api.DBPool)

	if err != nil {
		api.writeError(err, w)
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), DEFAULT_QUERY_TIMEOUT)

	var delId int
	err = api.DBPool.QueryRow(ctx, "DELETE FROM tag WHERE id=$1 RETURNING id", id).Scan(&delId)
	defer cancel()

	if err != nil {
		var pgErr *pgconn.PgError
		if errors.As(err, &pgErr) {
			api.writeError(&types.APIError{Code: 500, Err: fmt.Errorf("%s with code %s", pgErr.Message, pgErr.Code), Message: pgErr.Message}, w)
		} else {
			api.writeError(&types.APIError{Code: 500, Err: err, Message: "Error query database"}, w)
		}
		return
	}

	json.NewEncoder(w).Encode(types.APIResponseData{Code: 200, Data: tag})
}
