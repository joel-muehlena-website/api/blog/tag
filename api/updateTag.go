package api

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"strconv"
	"time"

	"github.com/gorilla/mux"
	"github.com/jackc/pgconn"
	"gitlab.com/joel-muehlena-website/api/blog/tag/types"
)

func (api *ApiHandler) updateTagbyId(w http.ResponseWriter, r *http.Request) {

	idParam := mux.Vars(r)["id"]

	id, err := strconv.Atoi(idParam)

	if err != nil {
		api.writeError(&types.APIError{Err: err, Code: 400, Message: "failed to parse your id param to int. The param was: " + idParam}, w)
		return
	}

	var tag types.Tag

	err = api.parseBody(r.Header.Get("Content-Type"), r, &tag)

	if err != nil {
		api.writeError(&types.APIError{Code: 400, Err: err, Message: "Wrong data received"}, w)
		return
	}

	err = api.Validator.Struct(tag)

	if err != nil {
		api.writeError(&types.APIError{Code: 400, Err: err, Message: "Wrong data received"}, w)
		return
	}

	fTag, err := getTagById(id, api.DBPool)

	if err != nil {
		api.writeError(err, w)
		return
	}

	tag.UpdatedAt = time.Now()

	ctx, cancel := context.WithTimeout(context.Background(), DEFAULT_QUERY_TIMEOUT)
	defer cancel()

	err = api.DBPool.QueryRow(ctx, "UPDATE tag SET name=$1, updatedat=$2 WHERE id=$3 RETURNING id", tag.Name, tag.UpdatedAt, id).Scan(nil)

	if err != nil {
		var pgErr *pgconn.PgError
		if errors.As(err, &pgErr) {
			api.writeError(&types.APIError{Code: 500, Err: fmt.Errorf("%s with code %s", pgErr.Message, pgErr.Code), Message: pgErr.Message}, w)
		} else {
			api.writeError(&types.APIError{Code: 500, Err: err, Message: "Error query database"}, w)
		}
		return
	}

	if id != -1 {
		tag.CreatedAt = fTag.CreatedAt
		tag.Id = fTag.Id

		json.NewEncoder(w).Encode(types.APIResponseData{Code: 200, Data: tag})
	} else {
		api.writeError(&types.APIError{Code: 500, Err: fmt.Errorf("failed to fetch the id of the newly created tag maybe an error occured"), Message: "Failed to fetch id"}, w)
	}

}
